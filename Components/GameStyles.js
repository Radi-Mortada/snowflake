import { StyleSheet, Dimensions, StatusBar } from "react-native";

const { width, height } = Dimensions.get("window");
const BOX_SIZE = width / 3;

const styles = StyleSheet.create({
  container: {
    paddingTop: StatusBar.currentHeight || 0,
    flex: 1,
    backgroundColor: "black",
  },
  list: {
    flexDirection: "column",
  },
  box: {
    width: BOX_SIZE,
    height: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  img: {
    width: BOX_SIZE,
    resizeMode: "stretch",
    height: height / 3,
  },
});

export default styles;
