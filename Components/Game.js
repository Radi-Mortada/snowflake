import React, { useReducer, useEffect, useCallback } from "react";
import { View, FlatList, Pressable, Image } from "react-native";
import _ from "lodash";
import styles from "./GameStyles";
import { ACTION_TYPES } from "../constants";
import Images from "../Images";
import reducer, { INITIAL_STATE } from "../reducer";
import StartGameModal from "./StartGameModal";

const { emptyImage } = Images;

const _keyExtractor = (item) => item.id;

const Game = () => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
  const gameStarted = state.get("gameStarted");

  const onStartGameClick = useCallback(() => {
    dispatch({ type: ACTION_TYPES.request });
  }, [dispatch]);

  useEffect(() => {
    if (gameStarted) {
      let fArray = _.map(state.get("originalBoxesArray"), "id");
      let cArray = _.map(state.get("currentBoxesArray"), "id");
      if (_.isEqual(fArray, cArray)) {
        alert("YOU HAVE WON! <3");
      }
    }
  }, [state, dispatch, gameStarted]);

  const checkMovable = useCallback(
    (index) => {
      const emptyIndex = state.get("emptyIndex");

      if (
        (index == 3 && emptyIndex == 2) ||
        (index == 2 && emptyIndex == 3) ||
        (index == 6 && emptyIndex == 5)
      ) {
        return false;
      }
      if (
        index === emptyIndex + 1 ||
        index === emptyIndex - 1 ||
        index === emptyIndex + 3 ||
        index === emptyIndex - 3
      ) {
        return true;
      }
    },
    [state]
  );

  const renderItem = useCallback(
    ({ item, index }) => {
      if (!item.empty) {
        return (
          <Pressable
            onPress={() => {
              if (checkMovable(index)) {
                dispatch({ type: ACTION_TYPES.setNext, nextIndex: index });
              }
            }}
            style={styles.box}
          >
            <Image source={item.img} resizeMode="cover" style={styles.img} />
          </Pressable>
        );
      } else {
        return (
          <Pressable style={[styles.box, { backgroundColor: "black" }]}>
            <View style={styles.img}>
              <Image
                source={emptyImage}
                resizeMode="contain"
                style={styles.img}
              />
            </View>
          </Pressable>
        );
      }
    },
    [dispatch, checkMovable]
  );

  return (
    <View style={styles.container}>
      <StartGameModal onStartClick={onStartGameClick} />
      <FlatList
        key={"_"}
        contentContainerStyle={styles.list}
        numColumns={3}
        keyExtractor={_keyExtractor}
        data={state.get("currentBoxesArray")}
        renderItem={renderItem}
      />
    </View>
  );
};

export default Game;
