import { Map } from "immutable";
import _ from "lodash";
import Images from "./Images";
import { ACTION_TYPES } from "./constants";

const { cat } = Images;

const buildArrayFromAssets = () => {
  const boxesArray = [];

  for (let index = 1; index < 10; index++) {
    boxesArray.push({
      id: index,
      title: `B${index}`,
      empty: index == 9,
      img: index !== 9 ? cat[index - 1] : null,
    });
  }

  return boxesArray;
};

const ORIGINAL_BOXES_ARRAY = buildArrayFromAssets();

export const INITIAL_STATE = Map({
  originalBoxesArray: ORIGINAL_BOXES_ARRAY,
  currentBoxesArray: ORIGINAL_BOXES_ARRAY,
  emptyIndex: ORIGINAL_BOXES_ARRAY.length - 1,
  gameStarted: false,
});

function reducer(state, action) {
  switch (action.type) {
    case ACTION_TYPES.request: {
      const boxesArray = ORIGINAL_BOXES_ARRAY;
      let emptyI = null;
      const randomize = _.shuffle(boxesArray);

      randomize.forEach((item, i) => {
        if (item.empty) {
          emptyI = i;
        }
      });

      return state.merge({
        originalBoxesArray: boxesArray,
        currentBoxesArray: randomize,
        emptyIndex: emptyI,
      });
    }
    case ACTION_TYPES.setNext: {
      const { nextIndex } = action;

      let emptyIndex = state.get("emptyIndex");
      const currentBoxesArray = state.get("currentBoxesArray");

      if (!emptyIndex) {
        currentBoxesArray.forEach((bx, index) => {
          if (bx.empty) {
            emptyIndex = index;
          }
        });
      }

      return state.merge({
        currentBoxesArray: currentBoxesArray.map((box, index) => {
          if (index == emptyIndex) {
            return {
              ...box,
              id: currentBoxesArray[nextIndex].id,
              img: currentBoxesArray[nextIndex].img,
              empty: !box.empty,
            };
          } else if (index === nextIndex) {
            return {
              ...box,
              id: currentBoxesArray[emptyIndex].id,
              img: currentBoxesArray[emptyIndex].img,
              empty: !box.empty,
            };
          }
          return box;
        }),
        emptyIndex: nextIndex,
        gameStarted: true,
      });
    }
    case ACTION_TYPES.move: {
      return state;
    }
    default:
      throw new Error();
  }
}

export default reducer;
