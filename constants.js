export const ACTION_TYPES = {
  initialize: "initialize",
  request: "request",
  setNext: "setNext",
  move: "move",
};
