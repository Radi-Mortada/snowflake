const IMAGES = {
  cat: [
    require("./Cat/image_part_001.png"),
    require("./Cat/image_part_002.png"),
    require("./Cat/image_part_003.png"),
    require("./Cat/image_part_004.png"),
    require("./Cat/image_part_005.png"),
    require("./Cat/image_part_006.png"),
    require("./Cat/image_part_007.png"),
    require("./Cat/image_part_008.png"),
    require("./Cat/image_part_009.png"),
  ],
  emptyImage: require("./snowflake_icon.png"),
};

export default IMAGES;
